# Error Handling is important with promises

**Error Handling :**
Error handling handles the runtime errors in the Javascript.

* If there is an error in one of our promises,error handling helps to identify the error without executing the further.

* Promise chains are helpful at error handling.
* If a promise rejects,the control jumps to the rejection handler.

For example :

There is an error in the code below.So,it throws an error


```
fetch('https://jsonplaceholder.typicode.com/uses').then((response) => {
    return response.json()
})
.catch((err) => {
    console.error(err)   // It throws TypeError: failed to fetch
})

```
We can see that the `.catch` may appear after one or may be several `.then`

* The easiest way to catch all errors is to append `.catch` to the end of chain

```
fetch('https://jsonplaceholder.typicode.com/users').then((response) => {
    return response.json()
}).then((data) => {
    console.log(data)
}).catch((error) => {
    console.error(error)
})

```
In the above code, `.catch` doesn't executes at all. But if any of the promises above rejects,then it would catch it.

## Implicit try...catch 

The code of a promise handlers has an invisible `try..catch` around it. If an error happens,it is treated as a rejection.

For example :
```
new Promise((resolve,reject) => {
    throw new Error("Something went wrong)
}).catch(alert);  // Error: Something went wrong
```
The above code works exactly the same as the below code.

```
new Promise((resolve,reject) => {
    reject(new Error("Something went wrong));
}).catch(alert);  // Error: Something went wrong
```



* `.catch` at the end of the chain is similar to `try..catch`. We may have as many handlers as we want,and then use a single `.catch` at the end to handle errors in all of them.
* In `try..catch` we can analyze the error and maybe rethrow if if it can't be handled.The samething is possible for promises.
* If we `throw` inside `.catch`, then the control goes to the next closest error handler.And if we handle the error and finish normally,then it continues to the next closest successful `.then` handler.

In the example below, the `.catch` successfully handles the error.

```
new Promise((resolve,reject) => {
    throw new Error("something went wrong");
}).catch((error) => {
    console.error(error)
}).then(() => {
    console.log("success)
})
```
Here the `.catch` block finishes normally.So the next `.then` handler is called

## What happens if an error is not handled?

If we forgot to append `.catch` to the end of the chain

```
new Promise(function() {
    demoFunction();  // Error here
})
.then(() => {
    
}); // without .catch at the end!
```
* If there is an error,the promise becomes rejected and it jumps to the closest rejection handler. So the error gets stuck. There is no code to handle it.


 